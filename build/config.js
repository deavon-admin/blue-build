var path			= require( 'path' ),
	rootDir     	= path.join( process.cwd() ),
	blueBuildDir    = path.join( __dirname, '..' ),

	rootPath    	= path.join.bind( path, rootDir ),
	blueBuildPath   = path.join.bind( path, blueBuildDir );

	// console.log( 'rootDir: ' + rootDir);
	// console.log( 'blueBuildDir: ' + blueBuildDir);

module.exports = {
	serverHost		: 'http://localhost',
	serverPort		: 9000,
	lrPort			: 35729,

	rootPath        : rootPath,
	blueBuildPath   : blueBuildPath,

	build: {
		path 		: rootPath.bind( rootPath, 'build' )
	},

	builder: {
		path 		: blueBuildPath.bind( blueBuildPath, 'build' )
	},

	dist: {
		path 		: rootPath.bind( rootPath, 'dist' )
	},

	spec: {
		path 		: rootPath.bind( rootPath, 'bower_components', 'blue-spec', 'dist' )
	},

	src: {
		path 		: rootPath.bind( rootPath, 'src' )
	},

	staging: {
		path 		: rootPath.bind( rootPath, 'staging' ),

		node: {
			path	: rootPath.bind( rootPath, 'staging', 'node' )
		},

		web: {
			path 	: rootPath.bind( rootPath, 'staging', 'web' )
		}
	},

	temp: {
		path 		: rootPath.bind( rootPath, 'temp' ),

		web: {
			path 	: rootPath.bind( rootPath, 'temp', 'web' )
		}
	},

	test: {
		path 		: rootPath.bind( rootPath, 'test' )
	},

	vendor: {
		path 		: rootPath.bind( rootPath, 'bower_components' )
	}
};

// module.exports = {

//     livereloadPort: 35729,
//     host: 'http://localhost',
//     serverPort: 9000, //Change port here

//     rootDir: rootDir,

//     src: { //App source path settings
//         root: rootDir + path.normalize('/src'),
//         jsPaths: '/src/js/**',
//         jsonPaths: '/src/json/**',
//         assetsPaths: '/src/assets/**',
//         htmlPaths: '/src/html/**',
//         hbsPaths: '/src/hbs/**/*.hbs',
//         test: '/test/',
//         testHbsPath: '/test/hbs/**/*.hbs',
//         testJsPath: '/test/js/**',
//         testDataPath: '/test/data/**'
//     },

//     staging: { //Staging path settings
//         root: rootDir + path.normalize('/staging'),
//         temp: rootDir + path.normalize('/staging/temp'),
//         frameworkOptTemp: rootDir + path.normalize('/staging/temp/blue/optimize'),
//         frameworkTplTemp: rootDir + path.normalize('/staging/temp/blue/template'),
//         app: rootDir + path.normalize('/staging/web'),
//         framework: rootDir + path.normalize('/staging/web/blue'),
//         frameworkTestPath: rootDir + path.normalize('/staging/web/blue/test'),
//         testLibRoot: rootDir + path.normalize('/staging/web/test'),
//         docsPath: '/docs',
//         jsPaths: '/js',
//         assetsPaths: '/assets',
//         dataPaths: '/data',
//         hbsPaths: '/js/template',
//         testLib: rootDir + path.normalize('/staging/web/test/lib'),
//         testHbsPath: '/test/js/template',
//         testJsPath: '/test/js',
//         testDataPath: '/test/data',
//         jsPattern: '/js/**/**.js',
// 		junitReportDirName: '/junit-report',
// 		junitTempDir: '/junit',
// 		junitTempHTMLPattern: '/**/*-j.html'
//     },
//     dist: { //dist path settings
//         root: rootDir + path.normalize('/dist'),
//         framework: rootDir + path.normalize('/dist/blue/js'),
//         jsPaths: '/'
//     },
//     build: { //Build library path settings
//         root: rootDir + path.normalize('/build'),
//         lib: rootDir + path.normalize('/build/lib'),
//         gulpLib: rootDir + path.normalize('/build/gulp-tasks/lib'),
//         hbsPath: rootDir + path.normalize('/build/hbs/specRunner.hbs'),
//         junitHbsPath: rootDir + path.normalize('/build/hbs/junitSpecRunner.hbs'),
//         ciHbsPath: rootDir + path.normalize('/build/hbs/ciSpecRunner.hbs'),
//         config: {
//             jasmine: {
//                 "core": rootDir + path.normalize("/build/lib/jasmine-2.0.0/js/jasmine"),
//                 "boot": rootDir + path.normalize("../lib/jasmine-2.0.0/js/console-boot"),
//                 "console": rootDir + path.normalize("../lib/jasmine-2.0.0/js/console")
//             },
//             ci: {
//             	"specRunner": rootDir + path.normalize("/build/lib/phantomjs-jasmine2/bin/phantomjs.runner.sh"),
//             	"specHtmlPhysicalPath": rootDir + path.normalize("/staging/web/blue/test/all.html"),
//             	"specHtmlWebPath": "/blue/test/all.html"
//             }
//         }
//     },
//     testCase: 'jasmine', //Define test case
//     jasmine: { //Jasmine path setting
//         root: '/test/lib/jasmine-2.0.0',
//         css: '/test/lib/jasmine-2.0.0/css/jasmine',
//         js: '/test/lib/jasmine-2.0.0/js',
//         frameworkJsPath: '/blue/js/index.js',
//         requireConfig: {
//             paths: {
//                 "jasmine": "/test/lib/jasmine-2.0.0/js/jasmine",
//                 "jasmine-html": "/test/lib/jasmine-2.0.0/js/jasmine-html",
//                 "jasmine-boot": "/test/lib/jasmine-2.0.0/js/web-boot",
//                 "jasmine-console": "/test/lib/phantomjs-jasmine/console-runner",
//                 "jasmine-console-reporter": "/test/lib/jasmine-console/consoleReporter",
//                 "jasmine-blanket": "/test/lib/blanket/blanket_jasmine.min",
//                 "jasmine-terminal": "/test/lib/jasmine-2.0.0/js/jasmine.terminal_reporter"
//             },
//             shim: {
//                 "jasmine": {
//                     "exports": "jasmine"
//                 },
//                 "jasmine-boot": {
//                     "deps": ["jasmine"],
//                     "exports": "jasmine"
//                 },
//                 "jasmine-html": {
//                     "deps": ["jasmine"],
//                     "exports": "jasmine"
//                 },
//                 "jasmine-terminal": {
//                     "deps": ["jasmine"],
//                     "exports": "jasmine"
//                 },
//                 "jasmine-console": {
//                     "deps": ["jasmine"],
//                     "exports": "jasmine"
//                 },
//                 "jasmine-blanket": {
//                     "deps": ["jasmine"],
//                     "exports": "jasmine"
//                 }
//             }
//         },

//         script: '\nrequire(["jquery", "jasmine-html", "jasmine-boot", "jasmine-blanket", "jasmine-terminal"], function($, jasmineHtml, jasmineBoot, jasmineBlanket ) {\n\tjasmine.getEnv().addReporter(new jasmineReporters.TerminalReporter({\n\t\tverbosity: 3,\n\t\tcolor: true\n\t}));\n\tvar specs = [];\n\tspecs.push("spec");\n\trequire(specs, function() {\n\t\twindow.onload();\n\t});\n});',
// 		scriptWithoutBlanket: '\nrequire(["jquery", "jasmine-html", "jasmine-boot", "jasmine-terminal"], function($, jasmineHtml, jasmineBoot ) {\n\tjasmine.getEnv().addReporter(new jasmineReporters.TerminalReporter({\n\t\tverbosity: 3,\n\t\tcolor: true\n\t}));\n\tvar specs = [];\n\tspecs.push("spec");\n\trequire(specs, function() {\n\t\twindow.onload();\n\t});\n});',
//     },
//     versionProperties: { //Define version
//         "version": "20149999",
//         "cibuildport": "8999"
//     },
//     optimizeSettings: {
//         logLevel: 1,
//         warnings: false,
//         mangle: false,
//         minify: false
//     },
//     optimizeSettingsForQA: {
//         logLevel: 1,
//         warnings: false,
//         mangle: true,
//         minify: true
//     }
// };
