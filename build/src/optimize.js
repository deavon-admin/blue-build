module.exports = function( options ){
	// jshint esnext:true
	var gutil		= require( 'gulp-util' ),
		path		= require( 'path' ),
		requirejs	= require( 'requirejs' ),
		through		= require( 'through2' ),
		vfs			= require( 'vinyl-fs' ),
		vm 			= require( 'vm' ),

		PluginError = gutil.PluginError;

	const PLUGIN_NAME = 'gulp-requirejs-optimize';

	options || ( options = {} );

	return through.obj( function( file, encoding, callback ){
		if( file.isNull() ){
			this.push( file );
			callback();
		}

		if( file.isStream() ){
			this.emit( 'error', new PluginError( PLUGIN_NAME, 'Streams are not supported' ) );
			callback();
		}

		if( file.isBuffer() ){
			var contents = file.contents.toString( encoding, 0, file.contents.length ),

				context = vm.createContext( { module: module } ),

				config = vm.runInContext( contents, context ),

				// Save original onModuleBundleComplete
				onModuleBundleComplete = config.onModuleBundleComplete,

				lookup = {};

			// Collect the bundles to push downstream after optimization
			config.onModuleBundleComplete = function( data ){
				lookup[ data.name ] = data.path;

				// Call original onModuleBundleComplete if defined
				onModuleBundleComplete && onModuleBundleComplete( data );
			};

			gutil.log( gutil.colors.bold( 'Optimizing with' ), file.path );
			gutil.log( '... From', config.baseUrl );
			gutil.log( '... To  ', config.dir || config.out );

			requirejs.optimize( config,

				// Success callback
				function( result ){
					gutil.log( gutil.colors.bold( 'Optimized' ) );

					result && result.length && gutil.log( result );

					// Multiple optimized files
					if( Array.isArray( config.modules ) && config.dir ){
						var files = [];

						config.modules.forEach( function( module ){
							module.name in lookup && files.push( path.join( config.dir, lookup[ module.name ] ) );
						} );

						vfs.src( files )
							.on( 'data', this.push.bind( this ) )
							.on( 'error', this.emit.bind( this, 'error' ) )
							.on( 'end', callback );

					// Single optimized file
					} else if( config.out ){
						vfs.src( config.out )
							.on( 'data', this.push.bind( this ) )
							.on( 'error', this.emit.bind( this, 'error' ) )
							.on( 'end', callback );
					}
				}.bind( this ),

				// Error callback
				function( error ){
					error && error.message && gutil.log( error.message );

					this.emit( 'error', new PluginError( PLUGIN_NAME, error, { showStack: true } ) );
				}.bind( this ) );
		}
	} );
};
