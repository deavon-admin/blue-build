var gutil = require('gulp-util'),
	path = require('path'),
	through = require('through2'),

	setDefaultOptions = function( opts ) {
		opts.indent = opts.indent || void 0;
		opts.noPackageJson = false;
		opts.noBowerJson = false;

		// fail gracefully
		if ( !opts.packageJson || !opts.packageJson.dependencies ) {
			opts.noPackageJson = true;
			opts.packageJson = {};
			opts.packageJson.dependencies = {};
		}

		if ( !opts.bowerJson || !opts.bowerJson.dependencies ) {
			opts.noBowerJson = true;
			opts.bowerJson = {};
			opts.bowerJson.dependencies = {};
		}

		return opts;
	},

	// Preserver new line at the end of a file
	possibleNewline = function ( json ) {
		var lastChar = (json.slice(-1) === '\n') ? '\n' : '';
		return lastChar;
	},

	// Figured out which "space" params to be used for JSON.stringfiy.
	space = function space( json ) {
		var match = json.match(/^(?:(\t+)|( +))"/m);
		return match ? (match[1] ? '\t' : match[2].length) : '';
	},

	updateKey = function( content, key, value, fileName ) {
		if( !content[key] ) {
			gutil.log('Warning: Adding new property to file: ', fileName, ' prop: ', key);
		}

		content[key] = value;
	};

// input release.json
// output updates package.json to package_tmp.json file OR
// output updates bower.json to bower_tmp.json file
module.exports = function(opts, version) {
	// set task options
	opts = setDefaultOptions(opts);
	var pkgJsonDepends = opts.packageJson.dependencies,
	    bowerJsonDepends = opts.bowerJson.dependencies,
	    noPackageJson = opts.noPackageJson,
	    noBowerJson = opts.noBowerJson,
		indent = opts.indent;

	var content, json, fileName;

	return through.obj(function(file, enc, cb) {
		if (file.isNull()) {
			return cb(null, file);
		}
		if (file.isStream()) {
			return cb(new gutil.PluginError('setversion', 'Streaming not supported'));
		}

		// is this package.json or bower.json
		fileName = path.basename(file.path).toLowerCase();
		gutil.log(fileName);

		// extract file contents
		json = file.contents.toString();
		try {
			content = JSON.parse(json);
		} catch (e) {
			return cb(new gutil.PluginError('setversion', 'Problem parsing JSON file', {fileName: file.path, showStack: true}));
		}

		// version
		updateKey( content, 'version', version, fileName);

		// update package.json dependencies?
		if ( fileName === 'package_tmp.json' ) {
			if ( noPackageJson ) {
				gutil.log('Info: No package.json dependency updates configured, setting empty dependendency section');
			}
			updateKey( content, 'dependencies', pkgJsonDepends, fileName );
		}

		// update bower.json dependencies?
		if ( fileName === 'bower_tmp.json' ) {
			if ( noBowerJson ) {
				gutil.log('Info: No bower.json dependency updates configured, setting empty dependendency section');
			}
			updateKey( content, 'dependencies', bowerJsonDepends, fileName );
		}

		file.contents = new Buffer(JSON.stringify(content, null, indent || space(json)) + possibleNewline(json));

		cb(null, file);
	});
};
