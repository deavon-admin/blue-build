module.exports = function( gulp, config, task ){
	var jshint	= require( 'gulp-jshint' ),
		stylish	= require( 'jshint-stylish' ),

		app = config.app;

	gulp.task( 'lint', 'Lint scripts in source & test directories.', task.lint = function(){
		return gulp.src([
			config.src.path( app, '**', 'js', '**', '*.js' ),
			config.test.path( app, '**', 'js', '**', '*.js' )
		])
			.pipe( jshint() )
			.pipe( jshint.reporter( stylish ) );
			//.pipe( jshint.reporter( 'fail' ) );
	}, { aliases: [ 'hint' ] } );
};
