module.exports = function( gulp, config, task ){
	var filter		= require( 'gulp-filter' ),
		forEach		= require( 'gulp-foreach' ),
		compile		= require( 'gulp-compile-handlebars' ),
		rename		= require( 'gulp-rename' ),
		path		= require( 'path' ),
		fs			= require( 'fs' );

	gulp.task( 'utils', false, task.utils = function(){
		return gulp.src( [ config.vendor.path( 'mout', 'src', '**', '*.js' ), '!' + config.vendor.path( 'mout', 'src', '*.js' ) ] )
			.pipe( forEach( function( stream, file ){
				var dotjs = path.extname( file.path ),
					fileName = path.basename( file.path, dotjs ),
					srcModule = file.path.replace( config.vendor.path( 'mout', 'src' ), 'mout' ).replace( dotjs, '' ),
					utilModule = srcModule.replace( 'mout', 'blue' ),
					utilPath = srcModule.replace( 'mout' + path.sep, '' ).replace( fileName, '' );

				return gulp.src( config.builder.path( 'hbs', 'util.hbs' ) )
					.pipe( compile( {
						srcModule: srcModule,
						utilModule: utilModule
					} ) )
					.pipe( rename( {
						basename: fileName,
						dirname: utilPath,
						extname: dotjs
					} ) )
					.pipe( filter( function( file ){
						var destFile = file.path.replace( config.builder.path( 'hbs' ), config.build.path( 'blue', 'js' ) );

						return !fs.existsSync( destFile );
					} ) )
					.pipe( gulp.dest( config.build.path( 'blue', 'js' ) ) )
					.pipe( gulp.dest( config.vendor.path( 'blue', 'js' ) ) );
			} ) );
	} );
};
