module.exports = function( gulp, config, task ){
	var es = require( 'event-stream' ),
		changed			= require( 'gulp-changed' ),
		defineModule	= require( 'gulp-define-module' ),
		gutil = require( 'gulp-util' ),
		path = require( 'path' ),
		precompileHandlebars	= require( 'gulp-handlebars' ),

		app = config.app;

	// based on gulp-handlebars pre-compilation function, adapted for Ractive
	precompileRactive = function(){
		return es.through(function(file) {
			var Ractive = require( 'ractive' );
			if (file.isNull()) {
				return this.queue(file);
			}

			if (file.isStream()) {
				return this.emit('error', new gutil.PluginError('gulp-ractive', 'Streaming not supported'));
			}

			var contents = file.contents.toString();
			var compiled = null;
			try {
				compiled = JSON.stringify( Ractive.parse(contents, {}) );
			}
			catch (err) {
				return this.emit('error', err);
			}

			file.contents = new Buffer(compiled);
			file.path = gutil.replaceExtension(file.path, '.js');

			this.queue(file);
		});
	};

	gulp.task( 'views-hbs', false, task[ 'views-hbs' ] = function(){
		return gulp.src( config.src.path( app, 'hbs', '**', '*.hbs' ) )
			.pipe( changed( config.build.path( app, 'js', 'template' ), { extension: '.js' } ) )
			// Precompiles templates, i.e. convert from .hbs to .js
			.pipe( precompileHandlebars() )
			// Define as AMD modules
			.pipe( defineModule( 'amd' ) )
			// Copy to /temp/<app>/js/template
			.pipe( gulp.dest( config.build.path( app, 'js', 'template' ) ) );
	} );

	gulp.task( 'views-ractive', false, [ 'views-hbs' ], task[ 'views-ractive' ] = function(){
		return gulp.src( config.src.path( app, 'template', '**', '*.html' ) )
			.pipe( changed( config.build.path( app, 'js', 'template' ), { extension: '.js' } ) )
			// Precompiles templates, i.e. convert from .hbs to .js
			.pipe( precompileRactive() )
			// Define as AMD modules
			.pipe( defineModule( 'amd' ) )
			// Copy to /temp/<app>/js/template
			.pipe( gulp.dest( config.build.path( app, 'js', 'template' ) ) );
	} );

	// 'Compile & publish templates from source to temporary.'
	gulp.task( 'views', false, task.views = function(){
		return es.concat(
		    task[ 'views-hbs' ](  ),
			task[ 'views-ractive' ](  )
		);
	} );
	// 'Perform a clean compile of templates & publish to temporary'
	gulp.task( 'views-clean', false, [ 'cleanbuild' ], task.views );
};
