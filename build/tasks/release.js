module.exports = function( gulp, config, task ){
	var es		= require( 'event-stream' ),
		git		= require( 'gulp-git' ),
		gutil 	= require( 'gulp-util' ),
		rename 	= require( 'gulp-rename' ),
		rimraf 	= require( 'gulp-rimraf' ),
		setversion = require('../src/setversion');


	// create a new release branch, update the package.json and bower.json files
	// dependency section

	var releaseConfig,
		releaseVersion;

	// create new release branch, fail if already exists
	gulp.task( 'rcreate-branch', false, function( cb ) {
		releaseConfig = require( config.rootPath( '/release.json' ) );
		releaseVersion = config.env.branch || releaseConfig.release.version;

		git.branch( releaseVersion, function (err) {
			cb( err );
		});
	});

	gulp.task( 'rcheckout-branch', false, [ 'rcreate-branch' ], function( cb ) {
		git.checkout( releaseVersion, function (err) {
			cb( err );
		});
	});

	gulp.task( 'rupdate-json', false, [ 'rcheckout-branch' ], function() {
		return es.concat(
			gulp.src( config.rootPath( '/package.json' ) )
				.pipe( rename({
					suffix: '_tmp',
					extname: '.json'
				}))
				.pipe( setversion( releaseConfig, releaseVersion ) )
				.pipe( gulp.dest( config.rootPath( '.' ) ) ),

			gulp.src( config.rootPath( '/bower.json' ) )
				.pipe( rename({
					suffix: '_tmp',
					extname: '.json'
				}))
				.pipe( setversion( releaseConfig, releaseVersion ) )
				.pipe( gulp.dest( config.rootPath( '.' ) ) )
		);
	});

	gulp.task( 'rcopy-json', false, [ 'rupdate-json' ], function(){
		return es.concat(
			gulp.src( config.rootPath( '/package_tmp.json' ) )
				.pipe( rename( '/package.json' ) )
				.pipe( gulp.dest( config.rootPath( '.' ) ) ),

			gulp.src( config.rootPath( '/bower_tmp.json' ) )
				.pipe( rename( '/bower.json' ) )
				.pipe( gulp.dest( config.rootPath( '.' ) ) )
		);
	} );

	gulp.task( 'rdelete-temp', false, [ 'rcopy-json' ], function(){
		return es.concat(
			gulp.src( config.rootPath( '/package_tmp.json' ), { read: false } )
				.pipe( rimraf( { force: true } ) ),
			gulp.src( config.rootPath( '/bower_tmp.json' ), { read: false } )
				.pipe( rimraf( { force: true } ) )
		);
	} );

	gulp.task('rcommit', false, [ 'rdelete-temp' ], function(){
	  return gulp.src([ config.rootPath( '/package.json' ),
	  					config.rootPath( '/bower.json' ) ])
	    .pipe(git.commit('commit for release: '+ releaseVersion));
	});

	gulp.task( 'release', 'create release, requires release.json file, optional --branch for branch override', [ 'rcommit' ], function(){
		return git.revParse( {args:'--abbrev-ref HEAD'}, function( err, hash ) {
			gutil.log( gutil.colors.bold( 'CURRENT BRANCH: ', hash ) );
			gutil.log( '' );
			gutil.log( gutil.colors.bold( 'ADDITIONAL TASKS: ' ) );
			gutil.log( 'gulp rcur' );
			gutil.log( 'gulp rbr --branch' );
			gutil.log( 'gulp rdel --branch' );
		});
	} );

	// convenience methods for manual cleanup

	// output the current branch we are
	gulp.task( 'release-current', 'show current branch', function() {
		return git.revParse( {args:'--abbrev-ref HEAD'}, function( err, hash ) {
			gutil.log( gutil.colors.bold( 'CURRENT BRANCH: ', hash ) );
		});
	}, {
		aliases: [ 'rcur' ]
	});

	gulp.task( 'release-branch', 'switch branch, gulp rbr --branch=develop',function( cb ) {
		git.checkout( config.env.branch, function ( err ) {
			cb( err );
		});
	}, {
		aliases: [ 'rbr' ]
	});

	gulp.task( 'release-delete', 'delete branch, gulp rdel --branch=x.y.z', function( cb ) {
		git.branch( config.env.branch, {args: "-D"}, function ( err ) {
			cb( err );
		});
	}, {
		aliases: [ 'rdel' ]
	});

};