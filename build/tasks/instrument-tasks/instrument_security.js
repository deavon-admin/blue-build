module.exports = function(gulp, config, task) {

    var shell = require('gulp-shell'),
        xml2json = require('gulp-xml2json'),
        rename = require('gulp-rename'),
        sec_scan_utils = require('./utils/security_scan_utils'),
        app = config.app;

    fortify_home = config.env.hasOwnProperty('fortify_home') ? config.env.fortify_home : "";

    //input to shell script
    // script path_to_js_source path_to_ouput release# projectName sealId
    gulp.task('fortify_scan', false,
        shell.task(
            [__dirname + '/fortify_scan.sh' + ' ' + config.src.path(app, "js") + ' ' + config.rootPath("instrument_reports", app, "security") + ' ' + app + ' ' + fortify_home])
    );

    gulp.task('fortify_xml_to_json', false, ['fortify_scan'], task.fortify_xml_to_json = function(cb) {

        gulp.src(config.rootPath("instrument_reports", app, "security", "*.xml"))
            .pipe(xml2json())
            .pipe(sec_scan_utils.cnvrtScrtyResult2JSON(app))
            .pipe(rename({
                suffix: "_security",
                extname: ".json"
            }))
            .pipe(gulp.dest(config.rootPath("instrument_reports", app, "security")))
            .on('end', function() {
                cb();
            })
    });

    gulp.task('security_scan', "Perform security scan on code", ['fortify_xml_to_json'], task.security_scan = function(cb) {
        cb();
    });

};