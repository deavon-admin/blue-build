module.exports = function(gulp, config, task) {
    var jshint = require('gulp-jshint'),
        stylish = require('jshint-stylish'),
        plato = require('gulp-plato'),
        app = config.app;

    gulp.task('ilint', 'Lint scripts in source.', task.ilint = function(cb) {

        var path = config.src.path(app, "js", "**", "*.js");

        return gulp.src(path)
            .pipe(plato(config.rootPath() + '/instrument_reports/' + app + '/quality'));

    });
};