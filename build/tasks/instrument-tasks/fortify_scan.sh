# Project Specific Configurations
# -------------------------------

# Common Configurations
# ----------------------
export SCAN_DIR=$1
export SCAN_DEST_DIR=$2
export RELEASE=$(date +"%m%d%Y_%H%M%S")
export SSC_APP_NAME=$3
export SEAL_ID=0

# set the path to fortify if its supplied, otherwise being set in a prior CI build step
if [[ $# -eq 4 ]]; then
	export FORTIFY_HOME=$4
else
	export FORTIFY_HOME=$FORTIFY_HOME
fi

cd ${SCAN_DIR}

# Fortify clean
${FORTIFY_HOME}/sourceanalyzer -clean -logfile ${SCAN_DEST_DIR}/${SSC_APP_NAME}-${RELEASE}_clean.log -b ${SSC_APP_NAME}-${RELEASE}

# Fortify translate JS
${FORTIFY_HOME}/sourceanalyzer  -64 -Xmx5120M -logfile ${SCAN_DEST_DIR}/${SSC_APP_NAME}-${RELEASE}_translate.log -b ${SSC_APP_NAME}-${RELEASE} -source 1.6 $SCAN_DIR

# Fortify scan
${FORTIFY_HOME}/sourceanalyzer   -64 -Xmx5120M  -logfile ${SCAN_DEST_DIR}/${SSC_APP_NAME}-${RELEASE}-scan.log -debug -verbose -b ${SSC_APP_NAME}-${RELEASE} -scan -f ${SCAN_DEST_DIR}/${SSC_APP_NAME}-${RELEASE}_${BUILD_ID}.fpr


${FORTIFY_HOME}/sourceanalyzer -b ${SSC_APP_NAME}-${RELEASE} -scan -project-template '${FORTIFY_HOME}/../Core/config/filters/SSAP_Project_Template_June3.xml' -f ${SCAN_DEST_DIR}/${SSC_APP_NAME}-${RELEASE}_${BUILD_ID}.fpr

${FORTIFY_HOME}/ReportGenerator -format xml -f ${SCAN_DEST_DIR}/${SSC_APP_NAME}-${RELEASE}.xml -source ${SCAN_DEST_DIR}/${SSC_APP_NAME}-${RELEASE}_${BUILD_ID}.fpr -template Detailed-DefaultReportDefinition.xml

# Fortify clean
${FORTIFY_HOME}/sourceanalyzer -Xmx3072M  -clean -logfile ${SCAN_DEST_DIR}/${SSC_APP_NAME}-${RELEASE}_clean.log -b ${SSC_APP_NAME}-${RELEASE}

