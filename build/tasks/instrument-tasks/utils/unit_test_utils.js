var gulp = require("gulp"),
    gutil = require('gulp-util'),
    crypto = require('crypto'),
    es = require('event-stream'),
    path = require('path'),
    fs = require('fs'),
    extend = require("util").extend,
    instrument_utils = require('./utils'),
    jsQltyClssfctnLkUpObj = require('../config/code_quality_classfctn.json'),
    config = require('../config/instrumentation_config.json'),
    testResObj = {
        "SuiteId": "",
        "SuiteDescription": "",
        "Description": "",
        "Duration": 0,
        "TestCaseStatusMessage": "",
        "FailedExpectations": [],
        "Result": ""
    };

function getTestObj_Mocha_JSONCov(test, status) {
    return {
        "SuiteId": test.title,
        "SuiteDescription": test.title,
        "Description": test.fullTitle,
        "Duration": test.duration,
        "TestCaseStatusMessage": "",
        "FailedExpectations": [],
        "Result": status
    };
};

function collectJSONCovOutput(file, cb, collectorObj, relatvieAppSrcPath) {
    var testReportObj = JSON.parse(file.contents),
        passes, failures, i, tmpObj;
    passes = testReportObj.passes || [];
    failures = testReportObj.failures || [];
    skipped = testReportObj.skipped || [];

    for (i in passes) {
        collectorObj.UnitTestCases.TestCase.push(getTestObj_Mocha_JSONCov(passes[i], config.UNIT_TEST_PASS_STATUS));
    }
    for (i in failures) {
        tmpObj = getTestObj_Mocha_JSONCov(failures[i], config.UNIT_TEST_FAIL_STATUS);
        tmpObj.FailedExpectations.push({
            "message": "",
            "stack": "",
            "passed": false
        });
        collectorObj.UnitTestCases.TestCase.push(tmpObj);
    }
    for (i in skipped) {
        collectorObj.UnitTestCases.TestCase.push(getTestObj_Mocha_JSONCov(skipped[i], config.UNIT_TEST_SKIP_STATUS));
    }

    return cb(null, file);
};

function collectsReultsXUNITOuput(collectorObj, relatvieAppSrcPath) {
    return es.map(function(file, cb) {
        var testReportObj = JSON.parse(file.contents),
            suite;

        //add all test cases in suites...they are already formatted
        for (var i = 0; i < testReportObj.length; i++) {
            suite = testReportObj[i];
            for (var j = 0; j < suite.testcase.length; j++) {
                collectorObj.UnitTestCases.TestCase.push(suite.testcase[j]);
            }
        }

        return cb(null, file);
    });
};

function mochaConverter(file, cb) {

    var utResObj = JSON.parse(file.contents);
    var testSuites = [],
        testCases,
        tmpSuiteObj,
        tmpTestCaseObj, tests;
    for (var suite in utResObj) {
        tmpSuiteObj = utResObj[suite]["$"];
        tmpSuiteObj["testcase"] = [];
        tests = (utResObj[suite] && utResObj[suite]["testcase"]) ? utResObj[suite]["testcase"] : [];

        for (var i = 0; i < tests.length; i++) {
            tmpTestCaseObj = tests[i];

            var obj = {
                "SuiteId": tmpSuiteObj["name"],
                "SuiteDescription": tmpSuiteObj["name"],
                "Description": tmpTestCaseObj["$"]["name"],
                "Duration": tmpTestCaseObj["$"]["time"],
                "TestCaseStatusMessage": tmpTestCaseObj["$"]["message"] || "",
                "FailedExpectations": [],
                "Result": "Pass"
            };

            if (typeof tmpTestCaseObj["failure"] !== 'undefined' && Array.isArray(tmpTestCaseObj["failure"])) {
                obj.Result = "Fail";
                for (var j = 0; j < tmpTestCaseObj["failure"].length; j++) {
                    obj.FailedExpectations.push({
                        "message": tmpTestCaseObj["failure"][j]["$"]["message"],
                        "stack": tmpTestCaseObj["failure"][j]["_"],
                        "passed": false
                    })
                }
            } else if (typeof tmpTestCaseObj["skipped"] !== 'undefined') {
                obj.Result = "Skipped";
            }

            tmpSuiteObj.testcase.push(obj);

        } //for test cases

        testSuites.push(tmpSuiteObj);
    } //for test suites

    file.contents = new Buffer(JSON.stringify(testSuites));

    //emit the modified test suite
    cb(null, file);
};

function collctUnitTestResults(collectorObj, relatvieAppSrcPath) {
    return es.map(function(file, cb) {
        return collectJSONCovOutput(file, cb, collectorObj, relatvieAppSrcPath);
    });
};


module.exports = {
    collctUnitTestResults: collctUnitTestResults
}