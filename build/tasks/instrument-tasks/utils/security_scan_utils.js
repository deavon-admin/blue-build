var crypto = require('crypto'),
    path = require('path'),
    es = require('event-stream'),
    instrument_utils = require('./utils');

var getReportSectionByTitle = function(sectionArray, titleText) {
    var retObj, matchFound = 0;
    for (var i = 0; i < sectionArray.length && !matchFound; i++) {
        if (sectionArray[i]["Title"][0] === titleText) {
            retObj = sectionArray[i];
            matchFound = !0;
        }
    }
    return retObj;
};

var fortifyXML2JSON = function(file, cb, appName) {
    var securityScanResObj = JSON.parse(file.contents);
    var resOutLine = getReportSectionByTitle(securityScanResObj.ReportDefinition.ReportSection, "Results Outline");
    var vulnrblytByCat = getReportSectionByTitle(resOutLine.SubSection, "Vulnerability Examples by Category");
    var issueListing = vulnrblytByCat["IssueListing"][0];
    var issueGroupings = issueListing["Chart"][0]["GroupingSection"] || [];
    var severity, message, fileName, filePath, line, snppt, issueObj;

    var securityQualityObj = {
        "Medium": {
            "Defects": []
        },
        "High": {
            "Defects": []
        },
        "Low": {
            "Defects": []
        }
    };

    for (var i = 0; i < issueGroupings.length; i++) {
        var issues = issueGroupings[i]["Issue"];

        for (var j = 0; j < issues.length; j++) {
            severity = issues[j]["Folder"][0];
            message = issues[j]["Abstract"][0];
            fileName = issues[j]["Primary"][0]["FileName"][0];
            filePath = path.join(app, issues[j]["Primary"][0]["FilePath"][0]);

            line = issues[j]["Primary"][0]["LineStart"][0];
            snppt = issues[j]["Primary"][0]["Snippet"][0];

            var hashInput = [fileName, filePath, line],
                id = instrument_utils.getMD5HashId(hashInput);

            issueObj = {
                "package": filePath,
                "module": fileName,
                "severity": severity,
                "message": message,
                "line": line,
                "type": "security",
                "id": id,
                "CodeUnits": [{
                    "line": line,
                    "column": "",
                    "package": filePath,
                    "file": fileName
                }]
            };

            securityQualityObj[severity]["Defects"].push(issueObj);
        }
    }

    file.contents = new Buffer(JSON.stringify(securityQualityObj));

    cb(null, file);
};

var cnvrtScrtyResult2JSON = function(appName) {
    return es.map(function(file, cb) {
        fortifyXML2JSON(file, cb, appName);
    });
};

var collctSecurityScanOutput = function(collectorObj) {
    return es.map(function(file, cb) {
        var securityScanResObj = JSON.parse(file.contents);
        collectorObj.High.Defects = collectorObj.High.Defects.concat(securityScanResObj.High.Defects);
        collectorObj.Medium.Defects = collectorObj.Medium.Defects.concat(securityScanResObj.Medium.Defects);
        collectorObj.Low.Defects = collectorObj.Low.Defects.concat(securityScanResObj.Low.Defects);
        return cb(null, file);
    });
};

module.exports = {
    cnvrtScrtyResult2JSON: cnvrtScrtyResult2JSON,
    collctSecurityScanOutput: collctSecurityScanOutput
}