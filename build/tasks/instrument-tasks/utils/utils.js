var crypto = require('crypto'),
    path = require('path'),
    es = require('event-stream'),
    http = require('http');

//send an arry and it will combine them and return a hash
function getMD5HashId(strArray) {
    //Crypto logic
    var shasum = crypto.createHash('md5'),
        hashStr = "";
    for (var i = 0; i < strArray.length; i++) {
        hashStr = hashStr + strArray[i];
    }
    shasum.update(hashStr);
    return shasum.digest('hex');
};

function getMaxNAvg(inputArr) {
    var sum = 0,
        avg = 0,
        max = 0;
    for (var i = 0; i < inputArr.length; i++) {
        sum = sum + inputArr[i];
        if (max < inputArr[i]) {
            max = inputArr[i];
        }
    }

    avg = sum / inputArr.length;

    return {
        "max": max,
        "avg": avg
    };
};

function postFinalJsonToERA(formatedJSON, urlConfig, cb) {

    // Setup the request.
    var req = http.request(urlConfig, function(res) {
        console.log("setup Post call request");
        res.setEncoding('utf-8');

        var responseString = '';

        res.on('data', function(data) {
            responseString += data;
        });

        res.on('end', function() {
            cb(undefined, responseString);
        });

    });

    req.on('error', function(e) {
        cb(e);
    });

    req.write(formatedJSON);
    req.end();
};

function extractMochaXUNITResults() {
    return es.map(function(file, cb) {
        var buf = file.contents,
            bufferString = buf.toString(),
            outputStr = "<testsuite></testsuite>",
            outputFromFile = "";
        var start = bufferString.indexOf("<testsuite"),
            end = bufferString.indexOf("</testsuite>");
        start = (start == -1) ? -1 : start;
        end = (end == -1) ? -1 : end + 12;
        outputFromFile = bufferString.substring(start, end);
        outputStr = (outputFromFile === "") ? outputStr : outputFromFile;
        file.contents = new Buffer(outputStr);
        cb(null, file);
    });
}

function extractMochaJSONCovResults() {
    return es.map(function(file, cb) {

        //try parsing it as JSON if exception then handle it 
        try {
            JSON.parse(file.contents);
        } catch (exception) {
            var buf = file.contents,
                bufferString = buf.toString(),
                outputStr = "";
            var start = bufferString.match(/\{[.\n\s"]*instrumentation/),
                end = bufferString.length;

            start = (start !== null) ? start.index : end;
            outputStr = bufferString.substring(start, end);
            outputStr = (outputStr === "") ? JSON.stringify({}) : outputStr;
            file.contents = new Buffer(outputStr);
        }
        cb(null, file);
    });
}

function getRelativeAppPath(srcPath, intrMediatDir) {
    //return logon when
    // srcPath => /Users/o601499/chase-foundation-m/instrument_reports/logon
    // intrMediatDir => instrument_reports
    return resPath = srcPath.split(intrMediatDir)[1] || "";
}

module.exports = {
    getMD5HashId: getMD5HashId,
    getMaxNAvg: getMaxNAvg,
    postFinalJsonToERA: postFinalJsonToERA,
    extractMochaXUNITResults: extractMochaXUNITResults,
    extractMochaJSONCovResults: extractMochaJSONCovResults,
    getRelativeAppPath: getRelativeAppPath
}