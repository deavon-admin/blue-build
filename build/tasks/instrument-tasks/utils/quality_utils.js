var gulp = require("gulp"),
    crypto = require('crypto'),
    es = require('event-stream'),
    path = require('path'),
    fs = require('fs'),
    instrument_utils = require('./utils'),
    jsQltyClssfctnLkUpObj = require('../config/code_quality_classfctn.json');

var collctPlatoOutput = function(collectorObj, relatvieAppSrcPath) {
    return es.map(function(file, cb) {
        var platoReportObj = JSON.parse(file.contents);
        //classify quality
        formatCodeQuality(platoReportObj, collectorObj, relatvieAppSrcPath)
        return cb(null, file);
    });
};

var formatCodeQuality = function(platoReportObj, collectorObj, appPath) {

    addCodeQualityReport(platoReportObj, collectorObj, appPath);
    addCodeComplexityReport(platoReportObj, collectorObj);

};

var addCodeQualityReport = function(platoReportObj, collectorObj, appPath) {
    var jshintMsgs = platoReportObj.jshint.messages;

    //loop through the jshint messages and add them
    for (var i = 0; i < jshintMsgs.length; i++) {
        jshintMsgs[i]["package"] = path.relative(appPath, platoReportObj.info.file);
        jshintMsgs[i]["file"] = path.basename(platoReportObj.info.fileShort);
        //set the severity and other properties on the message
        setJSLintMsgSeverity(jshintMsgs[i], jsQltyClssfctnLkUpObj);

        var hashInput = [];
        hashInput.push(jshintMsgs[i]["line"]);
        hashInput.push(jshintMsgs[i]["column"]);

        jshintMsgs[i]["id"] = instrument_utils.getMD5HashId(hashInput);

        jshintMsgs[i]["CodeUnits"] = [];
        jshintMsgs[i]["CodeUnits"].push({
            "line": jshintMsgs[i]["line"],
            "column": jshintMsgs[i]["column"],
            "package": jshintMsgs[i]["package"],
            "file": jshintMsgs[i]["file"]
        });

        //now add it to

        if (jshintMsgs[i].severity == "High") {
            collectorObj.CodeQuality.High.Defects.push(jshintMsgs[i]);
        } else if (jshintMsgs[i].severity == "Medium") {
            collectorObj.CodeQuality.Medium.Defects.push(jshintMsgs[i]);
        } else {
            collectorObj.CodeQuality.Low.Defects.push(jshintMsgs[i]);
        }
    }

    return;
};

var addCodeComplexityReport = function(platoReportObj, collectorObj) {
    var cmplxtyObj = platoReportObj.complexity;

    // //TODO as short term soln adding complexity in StyleQuality and making then all Med
    cmplxtyObj["severity"] = "Medium";
    cmplxtyObj["CodeUnits"] = [];
    cmplxtyObj["id"] = instrument_utils.getMD5HashId([])
    cmplxtyObj["message"] = "";
    collectorObj.StyleQuality.Medium.Defects.push(cmplxtyObj);

    collectorObj.complexityArr.push(platoReportObj.complexity.aggregate.complexity.cyclomatic);
    collectorObj.maintainabilityArr.push(platoReportObj.complexity.maintainability);

    return;
};

var setJSLintMsgSeverity = function(msgObj, lookUpObj) {
    var msgDesc = msgObj.message,
        matchFound = false,
        severity;

    for (var j = 0; j < lookUpObj.length && !matchFound; j++) {
        lkUpRegEx = new RegExp(lookUpObj[j]["regex"]);
        if (lkUpRegEx.test(msgDesc)) {
            severity = lookUpObj[j]["severity"];
            matchFound = true;
        }
    }

    if (!matchFound) {
        severity = "Low";
    }

    msgObj["severity"] = severity;
    return severity;
};


module.exports = {
    collctPlatoOutput: collctPlatoOutput
}