var gulp = require("gulp"),
    crypto = require('crypto'),
    es = require('event-stream'),
    path = require('path'),
    fs = require('fs'),
    instrument_utils = require('./utils'),
    jsQltyClssfctnLkUpObj = require('../config/code_quality_classfctn.json'),
    config = require('../config/instrumentation_config.json');

function collect_cov_istanbul(file, cb, collectorObj, relatvieAppSrcPath) {
    var coverageObj = JSON.parse(file.contents),
        cvrgDtls, stmtCvrg = 0,
        stmtTotal = 0,
        totallines = 0,
        brnchCvrg = 0,
        funCvrg = 0,
        linesCovered = 0,
        packageObj = {
            "name": app,
            "linecoverage": 0,
            "linesCovered": 0,
            "totallines": 0,
            "srcFile": []
        };

    //add all test cases in suites...they are already formatted
    for (var srcFile in coverageObj) {
        cvrgDtls = coverageObj[srcFile],
        coveredCount = 0;

        totallines = Object.keys(cvrgDtls.s).length;

        for (var l in cvrgDtls.s) {
            coveredCount = coveredCount + (cvrgDtls.s[l] > 0 ? 1 : 0);
        }
        linesCovered = coveredCount;
        stmtCvrg = parseFloat((linesCovered / totallines) * 100).toFixed(2);

        // coveredCount = 0;
        // //f=>function coverage
        // for (var l in cvrgDtls.f) {
        //     coveredCount = coveredCount + (cvrgDtls.f[l] > 0 ? 1 : 0);
        // }
        // funCvrg = parseFloat((coveredCount / Object.keys(cvrgDtls.f).length) * 100).toFixed(2);

        packageObj.srcFile.push({
            "name": cvrgDtls.path,
            "totallines": totallines,
            "linescovered": linesCovered,
            "linecoverage": stmtCvrg
        })

        //add to package level
        packageObj.totallines = packageObj.totallines + totallines;
        packageObj.linesCovered = packageObj.linesCovered + linesCovered;
    }

    packageObj.linecoverage = parseFloat((packageObj.linesCovered / packageObj.totallines) * 100).toFixed(2);

    collectorObj.Module.package.push(packageObj);

    console.log(collectorObj);

    return cb(null, file);
};

function collect_cov_jsoncov(file, cb, collectorObj, relatvieAppSrcPath) {
    var testReportObj = JSON.parse(file.contents),
        packageObj = {
            "name": "",
            "linecoverage": testReportObj.coverage || 0,
            "linescovered": testReportObj.hits || 0,
            "totallines": testReportObj.sloc || 0,
            "srcFile": []
        },
        i, files = testReportObj.files;

    for (i in files) {
        packageObj.srcFile.push({
            "name": instrument_utils.getRelativeAppPath(files[i].filename, relatvieAppSrcPath),
            "totallines": files[i].sloc,
            "linescovered": files[i].hits,
            "linecoverage": isNaN(files[i].coverage) ? "0" : files[i].coverage.toString()
        });
    }

    packageObj.name = instrument_utils.getRelativeAppPath(path.dirname(file.path), config.INSTRUMENT_REPORTS_PATH).replace(/[\/\\]/, "");

    collectorObj.Module.linescovered += packageObj.linescovered;
    collectorObj.Module.totallines += packageObj.totallines;
    collectorObj.Module.package.push(packageObj);
    return cb(null, file);
};

function collctCoverageOutput(collectorObj, relatvieAppSrcPath) {
    return es.map(function(file, cb) {
        return collect_cov_jsoncov(file, cb, collectorObj, relatvieAppSrcPath);
    });
};


module.exports = {
    collctCoverageOutput: collctCoverageOutput
}