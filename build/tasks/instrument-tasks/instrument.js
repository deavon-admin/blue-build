var gutil = require('gulp-util'),
    jshint = require('gulp-jshint'),
    stylish = require('jshint-stylish'),
    plato = require('gulp-plato'),
    fs = require('fs'),
    extend = require('util')._extend,
    rimraf = require('gulp-rimraf'),
    platoUtils = require('./utils/quality_utils'),
    utUtils = require('./utils/unit_test_utils'),
    cvrgUtils = require('./utils/coverage_utils'),
    securityUtils = require('./utils/security_scan_utils'),
    instrument_utils = require('./utils/utils'),
    instrument_config = require('./config/instrumentation_config');

module.exports = function(gulp, config, task) {
    var app = config.app;

    //TODO move this to a config file
    var qualityReportPathStr = instrument_config.QUALITY_REPORT_PATH_STR,
        cvrgReportPathStr = instrument_config.COVERAGE_REPORT_PATH_STR,
        unitTestReportPathStr = instrument_config.UNIT_TEST_REPORT_PATH_STR,
        securityReportPathStr = instrument_config.SECURITY_REPORT_PATH_STR,
        instrumentReportsPath = instrument_config.INSTRUMENT_REPORTS_PATH;

    var instrmntReportObj = require('./config/instrument_report_format.json');

    var appNames = app.split(","),
        platoReportPaths = [],
        cvrgReportPaths = [],
        unitTestRportPaths = [],
        securityRportPaths = [],
        maxCmplxty = 0,
        avgCmplxty = 0,
        avgMntnblty = 0,
        dt = new Date();

    var anlysisObj = {
        "buildId": 0,
        "release": null,
        "projectName": "TEST",
        "moduleName": "TEST",
        "language": "javascript",
        "timestamp": null,
        "status": "success",
        "tool": "Instrumentation"
    };

    for (var i = 0; i < appNames.length; i++) {
        platoReportPaths.push(config.rootPath(instrumentReportsPath, appNames[i], qualityReportPathStr));
        unitTestRportPaths.push(config.rootPath(instrumentReportsPath, appNames[i], unitTestReportPathStr));
        cvrgReportPaths.push(config.rootPath(instrumentReportsPath, appNames[i], cvrgReportPathStr));
        securityRportPaths.push(config.rootPath(instrumentReportsPath, appNames[i], securityReportPathStr));
    }

    gulp.task('clean_instrument_reports', false, task.clean_instrument_reports = function(cb) {
        gulp.src(config.rootPath(instrumentReportsPath), {
            read: false
        })
            .pipe(rimraf({
                force: true
            }))
            .on('end', function() {
                cb();
            })
    });

    gulp.task('add_quality_complexity', false, task.add_quality_complexity = function(cb) {
        instrmntReportObj.CodeMetrics.ERA.complexityArr = [];
        instrmntReportObj.CodeMetrics.ERA.maintainabilityArr = [];

        gulp.src(platoReportPaths)
            .pipe(platoUtils.collctPlatoOutput(instrmntReportObj.CodeMetrics.ERA, instrument_config.JS_SRC_DIR))
            .on('end', function() {
                var cmplxtyValues = instrument_utils.getMaxNAvg(instrmntReportObj.CodeMetrics.ERA.complexityArr);
                avgMntnblty = instrument_utils.getMaxNAvg(instrmntReportObj.CodeMetrics.ERA.maintainabilityArr).avg;

                //Maintainabiity Index
                instrmntReportObj.CodeMetrics.ERA.ApplicationMetrics.Metrics[0]["value"] = avgMntnblty;
                //Max Complexity
                instrmntReportObj.CodeMetrics.ERA.ApplicationMetrics.Metrics[1]["value"] = cmplxtyValues.max;
                //Average Complexity
                instrmntReportObj.CodeMetrics.ERA.ApplicationMetrics.Metrics[2]["value"] = cmplxtyValues.avg;

                delete instrmntReportObj.CodeMetrics.ERA.complexityArr;
                delete instrmntReportObj.CodeMetrics.ERA.maintainabilityArr;
                cb();
            });
    });

    gulp.task('add_ut_results', false, task.add_ut_results = function(cb) {
        gulp.src(unitTestRportPaths)
            .pipe(utUtils.collctUnitTestResults(instrmntReportObj.CodeMetrics.UnitTest))
            .pipe(cvrgUtils.collctCoverageOutput(instrmntReportObj.CodeMetrics.Coverage, "build"))
            .on('end', function() {
                var module = instrmntReportObj.CodeMetrics.Coverage.Module;
                module.linecoverage = (module.linescovered / module.totallines) * 100;
                module.linecoverage = isNaN(module.linecoverage) ? 0 : module.linecoverage;
                cb();
            });
    });


    //NOT USED as add_ut_results adds both test & coverage results
    gulp.task('add_coverage_results', false, task.add_coverage_results = function(cb) {
        gulp.src(cvrgReportPaths)
            .pipe(cvrgUtils.collctCoverageOutput(instrmntReportObj.CodeMetrics.Coverage, ""))
            .on('end', function() {
                cb();
            });
    });

    gulp.task('add_security_scan_results', false, task.add_security_scan_results = function(cb) {
        gulp.src(securityRportPaths)
            .pipe(securityUtils.collctSecurityScanOutput(instrmntReportObj.CodeMetrics.ERA.SecurityQuality, ""))
            .on('end', function() {
                cb();
            });
    });

    //'add_quality_complexity', 'add_security_scan_results','add_ut_results', 'add_coverage_results',
    gulp.task('instrument', 'Main instrumentation task', ['add_quality_complexity', 'add_security_scan_results', 'add_ut_results'], task.instrument = function(cb) {

        anlysisObj.projectName = (gutil.env && gutil.env.projectName) ? gutil.env.projectName : anlysisObj.projectName;
        anlysisObj.moduleName = (gutil.env && gutil.env.moduleName) ? gutil.env.moduleName : anlysisObj.moduleName;
        anlysisObj.buildId = (gutil.env && gutil.env.build) ? gutil.env.build.toString() : anlysisObj.buildId;

        anlysisObj.release = (dt.getMonth() + 1) + "-" + dt.getDate() + "-" + dt.getFullYear();
        anlysisObj.timestamp = (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear() + " " + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

        //note - node extend only does a shallow copy!!!, use it with caution
        instrmntReportObj["CodeMetrics"]["ERA"]["Analysis"] = extend({}, anlysisObj);
        instrmntReportObj["CodeMetrics"]["ERA"]["Analysis"]["tool"] = instrument_config.QLTY_CMPLXTY_TOOL;
        instrmntReportObj["CodeMetrics"]["Coverage"]["Analysis"] = extend({}, anlysisObj);
        instrmntReportObj["CodeMetrics"]["Coverage"]["Analysis"]["tool"] = instrument_config.CODE_COVERAGE_TOOL;
        instrmntReportObj["CodeMetrics"]["Coverage"]["Module"]["name"] = anlysisObj.moduleName;
        instrmntReportObj["CodeMetrics"]["UnitTest"]["Analysis"] = extend({}, anlysisObj);
        instrmntReportObj["CodeMetrics"]["UnitTest"]["Analysis"]["tool"] = instrument_config.UNIT_TEST_TOOL;

        fs.writeFileSync(config.rootPath(instrumentReportsPath, "instrumentation_report.json"), JSON.stringify(instrmntReportObj));
        gutil.log("Final instrumentation report is generated at " + config.rootPath(instrumentReportsPath, "instrumentation_report.json"));

    });
};