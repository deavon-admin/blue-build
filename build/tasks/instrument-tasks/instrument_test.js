module.exports = function(gulp, config, task) {
    var jshint = require('gulp-jshint'),
        stylish = require('jshint-stylish'),
        plato = require('gulp-plato'),
        istanbul = require('gulp-istanbul'),
        mochaPhantomJS = require('gulp-mocha-phantomjs'),
        xml2json = require('gulp-xml2json'),
        instrument_utils = require('./utils/utils'),
        unittest_utils = require('./utils/unit_test_utils'),
        instrument_config = require('./config/instrumentation_config'),
        app = config.app;

    //var requireDir = require('require-dir');

    //TODO move to config
    var instrumentReportsPtah = "instrument_reports",
        uniTestReportName = "unit_test_report";
    var testReportPath = config.rootPath(instrumentReportsPtah, app, uniTestReportName);


    gulp.task('convert_xunit_json', false, [], task.convert_xunit_json = function(cb) {

        //TODO move to config
        var instrumentReportsPtah = "instrument_reports",
            uniTestReportName = "unit_test_report";

        var testReportPath = config.rootPath(instrumentReportsPtah, app, uniTestReportName);

        //TODO remove extractMochaResults, //TEMP HACK TO CORRECT THE MOCHA OUTPUT
        gulp.src(testReportPath + ".xml ")
            .pipe(instrument_utils.extractMochaXUNITResults())
            .pipe(xml2json())
            .pipe(gulp.dest(config.rootPath(instrumentReportsPtah, app)))
            .pipe(unittest_utils.cnvrtUTResult2JSON())
            .pipe(gulp.dest(config.rootPath(instrumentReportsPtah, app)))
            .on('end', function() {
                cb();
            });
    });

    gulp.task('extract_ut_results', false, [], task.extract_ut_results = function(cb) {

        //TODO move to config
        var instrumentReportsPtah = "instrument_reports",
            uniTestReportName = "unit_test_report";

        var testReportPath = config.rootPath(instrumentReportsPtah, app, instrument_config.UNIT_TEST_REPORT_PATH_STR);

        //TODO remove extractMochaResults, //TEMP HACK TO CORRECT THE MOCHA OUTPUT
        gulp.src(testReportPath)
            .pipe(instrument_utils.extractMochaJSONCovResults())
            .pipe(gulp.dest(config.rootPath(instrumentReportsPtah, app)))
            .on('end', function() {
                cb();
            });
    });





}