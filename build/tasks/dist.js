module.exports = function( gulp, config, task ){
	var es = require( 'event-stream' ),
		rimraf	= require( 'gulp-rimraf' ),
		app = config.app;

	// Duplicate "test" task sequence with a dependence on "stage" to force correct order
	// of cleaning, building, staging, and testing
	gulp.task( 'distwebtest', false, [ 'docs' ], task.distwebtest = function(){
		return task.webtest();
	} );
	gulp.task( 'distwebtest-clean', false, [ 'docs-clean' ], task.distwebtest );

	gulp.task( 'distnodetest', false, [ 'distwebtest' ], task.distnodetest = function(){
		return task.nodetest();
	} );
	gulp.task( 'distnodetest-clean', false, [ 'distwebtest-clean' ], task.distnodetest );

	gulp.task( 'disttest', false, [ 'distnodetest' ], task.disttest = function(){
		return task.test();
	} );
	gulp.task( 'disttest-clean', false, [ 'distnodetest-clean' ], task.disttest );

	gulp.task( 'dist', 'Perform stage build, copy to dist folder', [ 'disttest' ], task.dist = function(){
		// Copy files from staging to distribution
		return gulp.src( [
				config.staging.web.path( '**' ),
				// Exclude build log, docs, and unit tests
				'!' + config.staging.web.path( 'build.txt' ),
				'!' + config.staging.web.path( app, 'docs' ) + '{,/**}',
				'!' + config.staging.web.path( app, 'test' ) + '{,/**}'
			] )
			.pipe( gulp.dest( config.dist.path() ) );
	} );

	gulp.task( 'distcopy', 'Copies files from staging to distribution.', task.distcopy = function(){
		// Copy files from staging to distribution
		return gulp.src( [
				config.staging.web.path( '**' ),
				// Exclude build log, docs, and unit tests
				'!' + config.staging.web.path( 'build.txt' ),
				'!' + config.staging.web.path( '*', 'docs' ) + '{,/**}',
				'!' + config.staging.web.path( '*', 'test' ) + '{,/**}'
			] )
			.pipe( gulp.dest( config.dist.path() ) );
	} );

    gulp.task( 'cleandist', false, task.cleandist = function(){
    	return gulp.src( config.dist.path(), { read: false } )
				.pipe( rimraf( { force: true } ) );
    } );

	gulp.task( 'dist-clean', 'Cleans, builds, & copies files from staging to distribution.', [ 'cleandist', 'disttest-clean' ], task.dist );
};
