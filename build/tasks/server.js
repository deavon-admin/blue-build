module.exports = function( gulp, config, task ){
	var connect		= require( 'connect' ),
		gutil		= require( 'gulp-util' ),
		http		= require( 'http' ),
		livereload	= require( 'gulp-livereload' ),
		open		= require( 'open' ),

		serverRoot	= config.env.root	|| config.staging.web.path(),
		serverHost	= config.env.host	|| config.serverHost,
		serverPort	= config.env.port	|| config.serverPort,
		lrPort		= config.env.lrPort	|| config.lrPort;

	gulp.task( 'server', 'Launch a web server.', task.server = function( done ){
		var middleware = connect()
				// Request logging
				.use( require( 'connect-logger' )() )

				// gzip compression
				.use( require( 'compression' )() )

				//Make serverRoot accessible to the public
				.use( connect.static( serverRoot ) );

		// Start server
		http.createServer( middleware )
			.listen( serverPort, done )
			.on( 'listening', function(){
				gutil.log( 'Started connect web server on ' + serverHost + ':' + serverPort + '.' );

				if( !config.env[ 'no-launch' ] ){
					open( serverHost + ':' + serverPort );
				}
			} )
			.on( 'error', function(){
				gutil.log( 'Web server '+ serverHost +':' + serverPort + ' is already running.' );
			} );
	} );

	gulp.task( 'run', 'Perform a clean build and launch a web server.', [ 'stage-clean' ], task.server );

	gulp.task( 'watch', 'Perform a clean build, launch a web server, & watch for changes. (UNSTABLE)', [ 'run' ], task.watch = function(){
		var lr = livereload( lrPort );

		// Watch source and perform a (dirty) build to staging
		gulp.watch( config.src.path( app, '**' ), [ 'stage' ] );

		// Watch staging and refresh the livereload server
		gulp.watch( config.staging.web.path( app, '**' ), function( file ){
			gutil.log( file.path, 'changed' );
			lr.changed( file.path );
		} );
	} );
};
