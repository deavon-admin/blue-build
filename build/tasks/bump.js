module.exports = function( gulp, config, task ){
	var es				= require( 'event-stream' ),
		bump			= require( 'gulp-bump' ),

		type			= config.env.prerelease || config.env.patch || config.env.minor || config.env.major,
		usageMessage	= 'Use --prerelease, --patch, --minor, --major to specify the type of bump.';

	// 'Bump the version of the project. ' + usageMessage
	gulp.task( 'bump', false, task.bump = function(){
		if( !type ){
			throw new TypeError( 'Bump type is undefined. ' + usageMessage );
		}

		return es.concat(
			// Bower
			gulp.src( config.path( 'bower.json' ) )
				.pipe( bump( { type: type } ) )
				.pipe( gulp.dest( config.path() ) ),

			// Node
			gulp.src( config.builder.path( 'package.json' ) )
				.pipe( bump( { type: type } ) )
				.pipe( gulp.dest( config.builder.path() ) ) );
	} );
};
