module.exports = function( gulp, config, task ){
	var	less	= require( 'gulp-less' )
		app	= config.app;

	gulp.task( 'less', 'Generates CSS from less.', task.less = function(){
		return gulp.src( config.src.path( app, 'less', '**', '*.less' ) )
			.pipe( less( ) )
			.pipe( gulp.dest( config.build.path( app, 'assets' ) ) )
    } );

	gulp.task( 'less-clean', 'Cleans and publishes documentation to staging.', task.less );

};
