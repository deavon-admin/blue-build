module.exports = function( gulp, config, task ){
	var es		= require( 'event-stream' ),
		rimraf	= require( 'gulp-rimraf' );

	gulp.task( 'clean', 'Removes build-related directories from project', task.clean = function(){
		// Remove distribution, staging, and temporary folders
		return es.concat(
			gulp.src( config.build.path(), { read: false } )
				.pipe( rimraf( { force: true } ) ),
			gulp.src( config.dist.path(), { read: false } )
				.pipe( rimraf( { force: true } ) ),
			gulp.src( config.staging.path(), { read: false } )
				.pipe( rimraf( { force: true } ) ),
			gulp.src( config.temp.path(), { read: false } )
				.pipe( rimraf( { force: true } ) )
		);
	} );
};
