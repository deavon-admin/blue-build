module.exports = function( gulp, config, task ){
	var jsdoc	= require( 'gulp-jsdoc' ),

		app		= config.app;

	gulp.task( 'docs', 'Publishes documentation to staging.', [ 'stage' ], task.docs = function(){
		return gulp.src( config.src.path( app, 'js', '**', '*.js' ) )
			.pipe( jsdoc.parser() )
			.pipe( jsdoc.generator(
				// Destination
				config.staging.web.path( app, 'docs' ),
				// Template
				{
				    path			: 'ink-docstrap',
				    systemName      : 'Blue.js',
				    footer          : 'Digital Framework',
				    copyright       : '&copy; ' + ( new Date() ).getFullYear() + ' JPMorgan Chase & Co.',
				    navType         : 'vertical',
				    theme           : 'spacelab',
				    linenums        : true,
				    collapseSymbols	: false,
				    inverseNav      : false
				},
				// Options
				{
					plugins: [ 'plugins/markdown' ]
				} ) );
	} );

	// Run docs task without executing stage dependency, makes docs publish much faster during development
	gulp.task( 'docs-only', false, function(){
		return task.docs();
	} );
	gulp.task( 'docs-clean', 'Cleans and publishes documentation to staging.', [ 'stage-clean' ], task.docs );

	gulp.task('docs-watch', 'Watches: src; Runs: docs (WITHOUT running stage dependency first)', function() {
		gulp.watch(config.src.path( app, '**' ), [ 'docs-only' ] );
	});
};
