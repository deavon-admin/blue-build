module.exports = function( gulp, config, task ){
	var rimraf	= require( 'gulp-rimraf' );

	// 'Erase all files in temp directory'
	gulp.task( 'cleantemp', false, task.cleantemp = function(){
		return gulp.src( config.temp.path(), { read: false } )
				.pipe( rimraf( { force: true } ) );
	} );

	// gulp.task( 'temp', [ 'views', 'app', 'test', 'vendor' ] );
	// gulp.task( 'temp-clean', [ 'views-clean', 'app-clean', 'test-clean', 'vendor-clean' ] );
};
