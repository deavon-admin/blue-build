module.exports = function( gulp, config, task ){
	var bower	= require( 'gulp-bower' );

	// 'Publish vendor libraries to temporary.'
	gulp.task( 'vendor', false, task.vendor = function() {
		// Publish vendor libraries to temp
		return bower()
			.pipe( gulp.dest( config.build.path() ) );
	});

	// 'Perform a clean publish of vendor libraries to temporary.'
	gulp.task( 'vendor-clean', false, [ 'cleanbuild' ], task.vendor );
};