module.exports = function( gulp, config, task ){
	var changed	= require( 'gulp-changed' ),
		es		= require( 'event-stream' ),
		concat	= require( 'gulp-concat' ),

		app		= config.app;

	// 'Publish an application from source to temporary.'
	gulp.task( 'app', false, task.app = function(){
		// Publish app from source to temporary folder
		return es.concat(
			// Assets
			gulp.src( config.src.path( app, 'assets', '**' ) )
				.pipe( changed( config.build.path( app, 'assets' ) ) )
				.pipe( gulp.dest( config.build.path( app, 'assets' ) ) ),

			// CONCAT PROP FILES in specific folder
			gulp.src( [ config.src.path( app, 'assets', 'locale/defaults.props' ), config.src.path( app, 'assets', 'locale/*_en.props' ) ] )
				.pipe( concat( 'language_en.props' ) )
				.pipe( gulp.dest( config.build.path( app, 'assets', 'locale/.' ) ) ),

			// CONCAT PROP FILES in specific folder
			gulp.src( [ config.src.path( app, 'assets', 'locale/defaults.props' ), config.src.path( app, 'assets', 'locale/*_es.props' ) ] )
				.pipe( concat( 'language_es.props' ) )
				.pipe( gulp.dest( config.build.path( app, 'assets', 'locale/.' ) ) ),

			// HTML
			gulp.src( config.src.path( app, '*.html' ) )
				.pipe( changed( config.build.path( app ) ) )
				.pipe( gulp.dest( config.build.path( app ) ) ),

			// JSON
			gulp.src( config.src.path( app, 'json', '**', '*.json' ) )
				.pipe( changed( config.build.path( app, 'data' ) ) )
				.pipe( gulp.dest( config.build.path( app, 'data' ) ) ),

			// Scripts
			gulp.src( config.src.path( app, 'js', '**', '*.js' ) )
				.pipe( changed( config.build.path( app, 'js' ) ) )
				.pipe( gulp.dest( config.build.path( app, 'js' ) ) ),

			// Index
			gulp.src( config.src.path( 'index.html' ) )
				.pipe( changed( config.build.path() ) )
				.pipe( gulp.dest( config.build.path() ) )
		);
	} );
	// 'Perform a clean publish of an application to temporary.'
	gulp.task( 'app-clean', false, [ 'cleanbuild' ], task.app );
};
