module.exports = function( gulp, config, task ){

	var gutil = require('gulp-util'),
		path = require('path'),
		rmdir = require('gulp-rimraf');

	gulp.task('init_removeOldLapisDirectory', false, [], task.init_removeOldLapisDirectory = function () {
		gutil.log('Removing blue directory');

		return gulp.src('./bower_components/blue', {read: false})
			.pipe(rmdir({force: true}));

	});

	gulp.task('init_copyFrameworkContents', false, ['init_removeOldLapisDirectory'], task.init_copyFrameworkContents = function () {
		gutil.log('Copying framework contents');

		return gulp.src('./bower_components/bluejs/dist/blue/**')
			.pipe(gulp.dest('bower_components/blue'));
	});

	gulp.task('init_removeOldBluejsFolder', false, ['init_copyFrameworkContents'], task.init_removeOldBluejsFolder = function () {
		gutil.log('Cleaning up all bluejs folders...');

		return gulp.src([
			path.join( config.vendor.path(), 'bluejs' ),
			path.join( config.staging.web.path(), 'bluejs' ),
			path.join( config.temp.web.path(), 'bluejs' )

		], {read: false})
			.pipe(rmdir({force: true}));
	});

	gulp.task('scaffold-init', 'build initialization', ['init_removeOldBluejsFolder'], task['scaffold-init'] = function () {
		gutil.log('Scaffold Init - Setting up framework');

	});
};
