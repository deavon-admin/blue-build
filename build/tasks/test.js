module.exports = function( gulp, config, task ){
	var es				= require( 'event-stream' ),
		fs              = require( 'fs' ),
 		gutil           = require( 'gulp-util' ),
		forEach			= require( 'gulp-foreach' ),
		compile			= require( 'gulp-compile-handlebars' ),
		handlebars      = require( 'gulp-handlebars' ),
		defineModule    = require( 'gulp-define-module' ),
		rename			= require( 'gulp-rename' ),
		path			= require( 'path' ),
		mochaPhantomJS  = require( 'gulp-mocha-phantomjs' ),
		gulpFilter          = require( 'gulp-filter' ),

		app				= config.app,

		filePathList    = [],
		testModuleList  = [],
		baseWebPath     = path.resolve( config.build.path( '' ) ),
		appTestAlias    = path.resolve( config.build.path( app, 'test' ) ).replace( config.build.path() + '/', '' ),
		appTestPath     = path.resolve( config.build.path( app, 'test', 'js' ) ).replace( config.build.path() + '/', '' ),
		appSrcPath     = path.resolve( config.build.path( app, 'js' ) ).replace( config.build.path() + '/', '' ),
        appTestDataPath = path.resolve( config.build.path(app, 'test', 'data')).replace(config.build.path() + '/', ''),
        coverageScript  = config.env.hasOwnProperty('coverage') ? '<script src="./blanket/dist/qunit/blanket.js" data-cover-only="['+ appSrcPath +']"></script>' : '',
        coverageEnabled = config.env.hasOwnProperty('coverage') ? 'true' : 'false',
        dataCoverOnly = [],
        testFolderGlobAbs = [ config.test.path( app, 'js', '**', '*.js' ) ],
        testFolderGlobRel,
        testFolderFilter;

    function clearPaths(){
    	filePathList = [  ];
    	testModuleList = [  ];
    }

    function addFilePath( filePath ){
    	filePathList.push( filePath );
    }

    function addModule( modulePath ){
    	testModuleList.push( modulePath );
    }

    function checkTestFolderOverride( ) {
    	if (config.env.testfolders) {

    		// sample --app=dashboard --testfolders=component/payments/quickPay,controller/payments/quickPay,lib/quickPay
    		testFolderGlobAbs = config.env.testfolders.split(',');
    		testFolderGlobRel = [];

    		testFolderGlobAbs.forEach( function(element, index, array) {
    			array[index] = config.test.path( app, 'js', element, '**', '*.js');
    			testFolderGlobRel[index] = element + '/**/*.js';
    			dataCoverOnly.push( appSrcPath + '/' + element );
    		});

    		// This filter later handles only copying testfolders to the build/test/js folder
    		testFolderFilter = gulpFilter( testFolderGlobRel );

    		coverageScript = '<script src="./blanket/dist/qunit/blanket.js" data-cover-only="['+ dataCoverOnly.join(',') +']"></script>';
    	}
    }

    gulp.task( 'pretest', false, [ 'build' ], task.pretest = function(){
    	checkTestFolderOverride();

		return gulp.src( testFolderGlobAbs, { read: false } )
			.pipe( gutil.buffer( function( err, files ){
				clearPaths(  );
				files.forEach( function( f ){
					addFilePath( f.path );
					addModule( app + '/test' +
						path.dirname( f.path ).replace( config.test.path( app, 'js' ), '' ) + '/' +
						path.basename( f.path ).split( '.' )[ 0 ] );
				} );

			} ) );
    } );
    gulp.task( 'pretest-clean', false, [ 'build-clean' ], task.pretest );

	// Construct specRunner files for each test script suitable for viewing/executing in a browser
	gulp.task( 'webtest', false, [ 'pretest' ], task.webtest = function(){
	    // Process .js files from test directory, generating specRunner
	    // for web view.
		return es.concat(
		    // Generate an all-in-one test runner for web view that runs all
		    // unit tests in one page. Useful for smoketesting browser compatibility errors.
			gulp.src( config.builder.path( 'hbs', 'specRunnerAll.hbs' ) )
				.pipe( compile( {
					testName: 'all',
					testList: testModuleList,
					appTestAlias: appTestAlias,
					appTestPath: appTestPath,
					appSrcAlias: app,
					appSrcPath: appSrcPath,
                    appTestDataPath: appTestDataPath,
                    coverageScript  : coverageScript,
	                coverageEnabled : coverageEnabled
				} ) )
				.pipe( rename( {
					basename: 'all',
					dirname:  '.',
					extname: '.html'
				} ) )
				.pipe( gulp.dest( config.build.path( app, 'test', 'web' ) ) ),

			gulp.src( testFolderGlobAbs )
				.pipe( forEach( function( stream, file ){
					return gulp.src( config.builder.path( 'hbs', 'specRunner.hbs' ) )
						.pipe( compile( {
							appTestAlias: appTestAlias,
							appTestPath: appTestPath,
							appSrcAlias: app,
							appSrcPath: appSrcPath,
							testName: path.basename( file.path ).split( '.' )[ 0 ],
							testModule: app + '/test' + path.dirname( file.path ).replace( config.test.path( app, 'js' ), '' ) + '/' + path.basename( file.path ).split( '.' )[ 0 ],
							testFullPath: file.path,
	                        appTestDataPath: appTestDataPath,
	                        coverageScript  : coverageScript,
	                        coverageEnabled : coverageEnabled
						} ) )
						.pipe( rename( {
							basename: path.basename( file.path ).split( '.' )[ 0 ],
							dirname: path.dirname( file.path ).replace( config.test.path( app, 'js' ), '' ),
							extname: '.html'
						} ) );
				} ) )
				.pipe( gulp.dest( config.build.path( app, 'test', 'web' ) ) ),

			// start copying from the original test path
			// testFolderFilter is for --testfolder option on the command line
			gulp.src( config.test.path( app, 'js', '**', '*.js' ) )
				.pipe( config.env.testfolders ? testFolderFilter : gutil.noop() )
				.pipe( gulp.dest( config.build.path( app, 'test', 'js' ) ) )
				.pipe( config.env.testfolders ? testFolderFilter.restore() : gutil.noop() ),

			gulp.src( config.test.path( app, 'data', '**', '*.json' ) )
				.pipe( gulp.dest( config.build.path( app, 'test', 'data' ) ) ),

			gulp.src( config.test.path( app, 'hbs', '**', '*.hbs' ) )
				// .pipe( changed( config.build.path( app, 'js', 'hbs' ), { extension: '.js' } ) )
				// Compiles templates, i.e. convert from .hbs to .js
				.pipe( handlebars() )
				.pipe( defineModule( 'amd' ) )
				// Copy to /build/<app>/js/template
				.pipe( gulp.dest( config.build.path( app, 'test', 'js', 'hbs' ) ) )

		);
	} );

	gulp.task( 'webtest-dev', 'Collates project files into build directory', ['build-dev'], task[ 'webtest-dev' ] = function() {
		// null task
		es.through( function write( data ) { this.emit( 'data', data ); }, function end() { this.emit('end'); } );
	});

	gulp.task( 'webtest-clean', [ 'pretest-clean' ], task.webtest );

	// Create specRunner file suitable for console execution
	gulp.task( 'nodetest', false, [ 'webtest' ], task.nodetest = function(){
		// Reporters:
		// spec ( default )
		// dot
		// nyan
		// list
		// progress
		// json-cov
		// html-cov
		// min
		// doc

		// Options for mochaPhantomJS
		var options = {
			reporter : config.env.reporter || 'spec',
		};

		// Instrumentation settings
		// ==========================
		var
			instrumentReportsPath = 'instrument_reports',
			uniTestReportName     = 'unit_test_report.json',
			testReportPath        = config.rootPath(instrumentReportsPath, app, uniTestReportName)
		;
        if (config.env.hasOwnProperty('instrument') && !fs.existsSync(config.rootPath(instrumentReportsPath, app))) {
            fs.mkdirSync(config.rootPath(instrumentReportsPath, app));
        }
		//moving this out so that report file is generated on —instrument
		if(config.env.hasOwnProperty('instrument')){
        	options.dump = testReportPath;
        }
        // ===========================

		return es.concat(

		    // Process .js files from test directory, generating single specRunner
		    // for PhantomJS use in the console.
			gulp.src( config.builder.path( 'hbs', 'phantomSpecRunner.hbs' ) )
				.pipe( compile( {
					testName: 'all',
					testList: filePathList,
					baseWebPath: baseWebPath,
					appTestAlias: appTestAlias,
					appTestPath: appTestPath,
					appSrcAlias: app,
					appSrcPath: appSrcPath,
                    appTestDataPath: appTestDataPath,
                    coverageScript  : coverageScript,
	                coverageEnabled : coverageEnabled
				} ) )
				.pipe( rename( {
					basename: 'all',
					dirname:  '.',
					extname: '.html'
				} ) )
				.pipe( gulp.dest( config.build.path( app, 'test', 'node' ) ) )
				.pipe( mochaPhantomJS( options ) )

		);
 	} );
	gulp.task( 'nodetest-clean', false, [ 'webtest-clean' ], task.nodetest );

	gulp.task( 'test', 'Executes all unit tests and builds web specRunner pages. Use --coverage to enable code coverage, --instrument to output report to instrumentation, and --reporter=json-cov for json coverage format. To clear all none stdout use JPMC_CI=true before the gulp command.', [ 'nodetest' ], task.test = function(){
		// return gulp.src( config.build.path( 'null' ) );
		es.through(function write(data) {
		    this.emit('data', data);
		    //this.pause()
		  },
		  function end () { //optional
		    this.emit('end');
		  });
	} );
	gulp.task( 'test-clean', 'Cleans and executes all unit tests and builds web specRunner pages.', [ 'nodetest-clean' ], task.test );

	gulp.task('test-watch', 'Watches: src, test; Runs: test', function() {
		gulp.watch(config.test.path( app, '**' ), [ 'test' ] );
		gulp.watch(config.src.path( app, '**' ), [ 'test' ] );
	});
 };
