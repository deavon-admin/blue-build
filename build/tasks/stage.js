/**
 * @module tasks/stage
 * @param  {Object} gulp The gulp runtime
 * @param  {PlainObject} config The gulp configuration
 * @param  {PlainObject} task A cache for storing task references
 */
module.exports = function (gulp, config, task) {
	var es = require('event-stream'),
		defineModule = require('gulp-define-module'),
		extend = require('node.extend'),
		optimize = require('../src/optimize'),
		path = require('path'),
		size = require('gulp-size'),
		tap = require('gulp-tap'),
		vm = require('vm'),
		rimraf = require('gulp-rimraf'),

		isQA,
		isIST,
		isDEV,
		isSingle,
		sourcemap,
		app,
		platform,
		settings,

		executeScript = function (file) {
			var fileContents = file.contents.toString('utf8', 0, file.contents.length),
				fileContext = vm.createContext({ module: module });

			return vm.runInContext(fileContents, fileContext);
		},

		configure = function (properties) {
			return tap(function (file) {
				var build = executeScript(file),

					configured = extend(true, build, properties);

				// DEV builds only produce an main.js
				if (isDEV) {
					var filteredModules = configured.modules.filter(function (module) {
						return module.name === app + '/main';
					});
					configured.modules = filteredModules;
				}

				file.contents = new Buffer(JSON.stringify(configured));
			});
		},

		setSettings = function () {

			isSingle = config.env.single || false;
			isQA = config.env.qa || false;
			isIST = config.env.ist || false;
			isDEV = config.env.dev || ( !isIST && !isQA );
			sourcemap = config.env.map || false;

			app = config.app;
			platform = config.env.platform || 'web'; // "web" or "node"

			// Build Settings
			settings = {
				// /temp
				// baseUrl					: path.relative( config.blueBuildPath(), config.temp.path() ),
				baseUrl: config.build.path(),
				// dir						: path.relative( config.blueBuildPath(), config.staging.path( platform ) ),
				dir: config.temp.path(platform),
				generateSourceMaps: sourcemap,
				mainConfigFile: config.src.path(app, 'js', 'config.js'),
				modules: [],
				optimize: isDEV ? 'none' : 'uglify2',
				optimizeCss: 'none',
				preserveLicenseComments: false,
				uglify2: {
					// http://lisperator.net/uglifyjs/compress
					compress: {
						sequences: true, // join consecutive statemets with the “comma operator”
						properties: true, // optimize property access: a["foo"] → a.foo
						dead_code: true, // discard unreachable code
						drop_debugger: true, // discard “debugger” statements
						unsafe: false, // some unsafe optimizations (see below)
						conditionals: true, // optimize if-s and conditional expressions
						comparisons: true, // optimize comparisons
						evaluate: true, // evaluate constant expressions
						booleans: true, // optimize boolean expressions
						loops: true, // optimize loops
						unused: true, // drop unused variables/functions
						hoist_funs: true, // hoist function declarations
						hoist_vars: false, // hoist variable declarations
						if_return: true, // optimize if-s followed by return/continue
						join_vars: true, // join var declarations
						cascade: true, // try to cascade `right` into `left` in sequences
						side_effects: true, // drop side-effect-free statements
						warnings: false	  // warn about potentially dangerous optimizations/code
					},
					// http://lisperator.net/uglifyjs/codegen
					output: {
						ascii_only: true,
						beautify: !isQA
					},
					mangle: isQA
				},
				useSourceUrl: false
			};
		};

	gulp.task('tempcopy', false, [ 'cleantemp', 'webtest'], task.tempcopy = function () {

		setSettings();

		return gulp.src(config.build.path('**', '*'))
			.pipe(gulp.dest(config.temp.path(platform)));
	});

	gulp.task('tempcopy-clean', false, [ 'webtest-clean', 'cleanstage' ], task.tempcopy);

	gulp.task('buildoptimized', false, [ 'tempcopy' ], task.buildoptimized = function () {
		return gulp.src(config.src.path(app, 'build.js'))
			// Configure the build script
			.pipe(configure(settings))
			// Restore the module definition stripped by configuring it
			.pipe(defineModule('node'))
			// Run the build script through r.js
			.pipe(optimize())
			// // Copy to staging directory
			// .pipe( gulp.dest( config.staging.path( platform ) ) )
			// Calculate uncompressed size
			.pipe(size({
				showFiles: true,
				title: 'Module'
			}))
			// Calculate compressed size
			.pipe(size({
				gzip: true,
				showFiles: true,
				title: 'Module'
			}));
	});

	gulp.task('buildoptimized-clean', false, [ 'tempcopy-clean' ], task.buildoptimized);

	gulp.task('stage-delete-app', false, [ 'buildoptimized' ], task['stage-delete-app'] = function () {
		if ( isSingle ) {
			// patch to support multi-app IST builds
			// single only replaces one app in staging instead of all apps
			// useful when running multiple stage commands for different apps
			return gulp.src( config.staging.path( platform, app ), { read: false })
				.pipe(rimraf({ force: true }));
		} else {
			return;
		}
	});

	gulp.task('stage-prep', false, [ 'stage-delete-app' ], task['stage-prep'] = function () {
		if ( isSingle ) {
			// copy only current app to staging
			return gulp.src( config.temp.path( platform, app, '**', '*') )
				.pipe(gulp.dest(config.staging.path( platform, app )));
		} else {
			// copy all files from "temp" to staging
			return gulp.src(config.temp.path('**', '*'))
				.pipe(gulp.dest(config.staging.path()));
		}
	});

	//This task actually tidies up AFTER staging (not before)
	gulp.task('stage', 'Publish all project files to staging', ['stage-prep'], task.stage = function () {
		return gulp.src([
			path.join(config.build.path(), 'bluejs'),
			path.join(config.vendor.path(), 'bluejs'),
			path.join(config.staging.web.path(), 'bluejs'),
			path.join(config.temp.web.path(), 'bluejs')

		], {read: false})
			.pipe(rimraf({force: true}));
	});

	gulp.task('stage-dev', 'Publish all project files to staging', ['webtest-dev'], task[ 'stage-dev' ] = function () {
		platform = config.env.platform || 'web'; // "web" or "node"

		task.utils();
		return gulp.src(config.build.path('**', '*'))
			.pipe(gulp.dest(config.staging.path(platform)));
	});

	gulp.task('cleanstage', false, task.cleanstage = function () {
		return es.concat(
			gulp.src(config.staging.path(), { read: false })
				.pipe(rimraf({ force: true })),
			gulp.src(config.temp.path(), { read: false })
				.pipe(rimraf({ force: true }))
		);
	});

	/**
	 * Cleans the current distribution, staging, and temporary folders, then builds an application and publishes it to staging
	 * @example
	 * // Stage an IST build of blue.js
	 * gulp stage-clean --app=blue --ist
	 */
	gulp.task('stage-prep-clean', false, [ 'buildoptimized-clean' ], task['stage-prep']);

	gulp.task('stage-clean', 'Perform a clean build & publish to staging.', [ 'stage-prep-clean'], task.stage);

	gulp.task('stage-watch', 'Watches: src, test; Runs: stage', function () {
		setSettings();

		gulp.watch(config.test.path(app, '**'), [ 'stage-dev' ]);
		gulp.watch(config.src.path(app, '**'), [ 'stage-dev' ]);
	});
};