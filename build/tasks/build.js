module.exports = function( gulp, config, task ){
	var es = require( 'event-stream' ),
		gutil = require( 'gulp-util' ),
		rimraf	= require( 'gulp-rimraf' );

	gulp.task( 'build-step-1-dev', false, [ 'views', 'app', 'less' ], task['build-step-1-dev'] = function(){
		es.through( function write( data ) { this.emit( 'data', data ); }, function end() { this.emit('end'); } );
	} );
	gulp.task( 'build-step-1', false, [ 'views', 'app', 'vendor', 'less' ], task['build-step-1'] = function(){
		es.through( function write( data ) { this.emit( 'data', data ); }, function end() { this.emit('end'); } );
	} );
	gulp.task( 'build-step-1-clean', false, [ 'views-clean', 'app-clean', 'vendor-clean', 'less-clean' ], task['build-step-1'] );

	gulp.task( 'build-init_removeOldLapisDirectory', false, [ 'build-step-1' ], task['build-init_removeOldLapisDirectory'] = function() {
		return task['init_removeOldLapisDirectory']();
	} );
	gulp.task( 'build-init_removeOldLapisDirectory-clean', false, [ 'build-step-1-clean' ], task['build-init_removeOldLapisDirectory'] );

	gulp.task( 'build-init_copyFrameworkContents', false, [ 'build-init_removeOldLapisDirectory' ], task['build-init_copyFrameworkContents'] = function() {
		return task['init_copyFrameworkContents']();
	} );
	gulp.task( 'build-init_copyFrameworkContents-clean', false, [ 'build-init_removeOldLapisDirectory-clean' ], task['build-init_copyFrameworkContents'] );

	gulp.task( 'build-init_removeOldBluejsFolder', false, [ 'build-init_copyFrameworkContents' ], task['build-init_removeOldBluejsFolder'] = function() {
		return task.init_removeOldBluejsFolder();
	} );
	gulp.task( 'build-init_removeOldBluejsFolder-clean', false, [ 'build-init_copyFrameworkContents-clean' ], task[ 'build-init_removeOldBluejsFolder' ] );

	gulp.task( 'build-scaffold-init', false, [ 'build-init_removeOldBluejsFolder' ], task['build-scaffold-init'] = function() {
		return task['scaffold-init']();
	} );
	gulp.task( 'build-scaffold-init-clean', false, [ 'build-init_removeOldBluejsFolder-clean' ], task['build-scaffold-init'] );

	// 'Collates project files into build directory'
	gulp.task( 'build', false, ['build-scaffold-init'], task.build = function() {
		return  task.utils();
		// // null task
		// es.through( function write( data ) { this.emit( 'data', data ); }, function end() { this.emit('end'); } );
	});

	// 'Collates project files into build directory'
	gulp.task( 'build-dev', false, ['build-step-1-dev'], task.build = function() {
		return task.utils();
		// // null task
		// es.through( function write( data ) { this.emit( 'data', data ); }, function end() { this.emit('end'); } );
	});

	gulp.task( 'cleanbuild', false, task.cleanbuild = function(){
		return gulp.src( config.build.path(), { read: false } )
			.pipe( rimraf( { force: true } ) );
	} );

	// 'Cleans and collates project files into build directory'
	gulp.task( 'build-clean', false, [ 'build-scaffold-init-clean' ], task.build );
};
