var config	= require( './build/config' ),
	glob	= require( 'glob' ),
	gulp	= require( 'gulp' ),
	gutil	= require( 'gulp-util' ),
	help	= require( 'gulp-help' ),
	path	= require( 'path' ),

	// Cache for Gulp tasks. Used to workaround Gulp's dependency resolution
	// limitations. Should no longer be needed with Gulp 4.
	task 	= {};

config.env = gutil.env;

// If application is not specified, use the first one in source
config.app = config.env.app || path.basename( glob.sync( config.src.path( '*' + path.sep ) )[ 0 ] );

// Apply help to gulp
help( gulp, { aliases: [ '?', 'h' ] } );

// Load Base Tasks
glob.sync( config.builder.path( 'tasks', '*.js' ) ).forEach( function( taskPath ){
	// Pass gulp and config into every gulp task
	require( taskPath )( gulp, config, task );
} );
glob.sync( config.builder.path( 'tasks', 'instrument-tasks', '*.js' ) ).forEach( function( taskPath ){
	// Pass gulp and config into every gulp task
	require( taskPath )( gulp, config, task );
} );

gulp.task( 'default', [ 'help' ] );
