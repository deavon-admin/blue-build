{
	// See http://jshint.com/docs/ for more details

	// Express or Suppress specific warnings
	// Use +/- instead of true/false to indicate expression vs. suppression.
	// https://github.com/jshint/jshint/blob/master/src/messages.js
	"-W099"			: true,	    // +: express "Mixed spaces and tabs."
	"-W015"			: true,     // +: express "Expected '{a}' to have an indentation at {b} instead at {c}."
	"-W018"			: true,		// +: express "Confusing use of '{a}'."
	"+W070"			: true,		// +: express "Extra comma. (it breaks older versions of IE)"

	// Settings
    "passfail"      : false,	// true: Stop on first error
    "maxerr"        : 50,		// {int} Maximum error before stopping.

    // Development
    "debug"         : true,		// true: Allow debugger statements e.g. browser breakpoints.
    "devel"         : true,		// true: Allow developments statements e.g. `console.log()`, `alert()`, etc.

    // EcmaScript 5
    "globalstrict"  : false,	// true: Allow global `use strict` (also enables strict).
    "strict"        : false,	// true: Require `use strict` pragma  in every file.

    // EcmaScript 6
    "esnext"		: false,	// true: Allow EcmaScript 6 syntax, e.g. `const`

    // Code Configuration
    "asi"           : false,	// true: Tolerate Automatic Semicolon Insertion (no semicolons).
	"bitwise"       : true,		// true: Prohibit bitwise operators (&, |, ^, etc.).
	"boss"          : true,		// true: Tolerate assignments inside if, for & while. Usually conditions & loops are for comparison, not assignments.
	"camelcase"     : false,    // true: Identifiers must be in camelCase.
	"curly"         : true,		// true: Require {} for every new block or scope.
	"eqeqeq"        : true,		// true: Require triple equals i.e. `===`.
	"eqnull"        : true,		// true: Tolerate use of `== null`.
	"evil"          : false,	// true: Tolerate use of `eval`.
    "expr"          : true,		// true: Tolerate `ExpressionStatement` as Programs.
	"forin"         : true,		// true: Require filtering `for in` loops with `hasOwnProperty`.
	"funcscope"		: false,	// true: Tolerate defining variables inside control statements.
	"immed"         : true,		// true: Require immediate invocations to be wrapped in parens e.g. `( function(){}() );`.
	"indent"        : false,	// {int} Number of spaces to use for indentation
	"iterator"      : false,	// true: Tolerate using the `__iterator__` property
    "lastsemic"     : false,	// true: Tolerate omitting a semicolon for the last statement of a 1-line block
	"latedef"       : true,		// true: Require variables/functions to be defined before being used.
	"laxbreak"      : false,	// true: Tolerate unsafe line breaks e.g. `return [\n] x` without semicolons.
	"laxcomma"      : true,		// true: Tolerate comma-first coding style.
	"loopfunc"      : false,	// true: Tolerate functions being defined in loops.
	"maxparams"		: false,	// {int} Max number of formal params allowed per function
    "maxdepth"		: false,	// {int} Max depth of nested blocks (within functions)
    "maxstatements"	: false,	// {int} Max number statements per function
    "maxcomplexity"	: false,	// {int} Max cyclomatic complexity per function
    "maxlen"		: false,	// {int} Max number of characters per line
    "multistr"      : false,	// true: Tolerate multi-line strings
	"newcap"        : true,		// true: Require capitalization of all constructor functions e.g. `new F()`.
	"noarg"         : true,		// true: Prohibit use of `arguments.caller` and `arguments.callee`.
	"noempty"       : true,		// true: Prohibit use of empty blocks.
	"nomen"         : false,	// true: Prohibit trailing `_` in variables
	"nonew"         : false,	// true: Prohibit use of constructors for side-effects (without assignment)
	"onevar"        : false,	// true: Allow only one `var` statement per function
	"plusplus"      : false,	// true: Prohibit use of `++` & `--`
	"proto"         : true,		// true: Tolerate using the `__proto__` property
	"quotmark"      : "single",	// Quotation mark consistency:
								// false	: do nothing (default)
								// true		: ensure whatever is used is consistent
								// "single"	: require single quotes
								// "double"	: require double quotes
	"scripturl"     : false,	// true: Tolerate script-targeted URLs.
    "smarttabs"		: false,	// true: Tolerate mixed tabs/spaces when used for alignment
    "shadow"        : false,	// true: Allows re-define variables later in code e.g. `var x=1; x=2;`.
    "sub"           : false,	// true: Tolerate using `[]` notation when it can still be expressed in dot notation.
    "supernew"      : false,	// true: Tolerate `new function () { ... };` and `new Object;`.
	"trailing"      : false,	// true: Prohibit trailing whitespaces.
	"undef"			: true,		// true: Require all non-global variables to be declared (prevents global leaks)
    "unused"		: true,		// true: Require all defined variables be used
    "validthis"     : false,	// true: Tolerate using this in a non-constructor function
	"white"         : false,	// true: Check against strict whitespace and indentation rules.

	// Environment Globals
    "browser"		: true,		// Standard browser globals e.g. `window`, `document`, etc.
    "couch"			: false,	// CouchDB globals.
    "dojo"			: false,	// Dojo Toolkit globals.
    "jquery"		: true,		// jQuery globals.
    "mootools"		: false,	// MooTools globals.
    "node"			: true,		// Node.js globals.
    "nonstandard"	: false,	// Non-standard, widely adopted globals, e.g. `escape`, `unescape`, etc.
    "prototypejs"	: false,	// Prototype and Scriptaculous globals.
    "rhino"			: false,	// Rhino globals.
    "worker"		: false,	// Web Worker globals.
    "wsh"			: false,	// Windows Script Host globals.
    "yui"			: false,	// YUI globals.

    // Custom Globals
    "predef"		: [
    	// ES6
    	"Map",
    	"Promise",
    	"Set",
    	"WeakMap",

    	// AMD API
    	"define",
		"require",
		"requirejs",

		// BDD API
		"afterEach",
		"beforeEach",
		"describe",
		"expect",
		"it",
		"waitsFor",
		"runs",
		"spyOn"
    ]
}